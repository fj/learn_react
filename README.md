# learn\_react

This repository contains my source code for the "Try React" project. The
guideline is available in
[this](https://reactjs.org/docs/getting-started.html#try-react) page.

## License

Please refer to the `LICENSE` file.
