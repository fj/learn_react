import React from 'react'
import ReactDOM from 'react-dom/client'
import './index.css'

function Square(props) {
  return (
    <button
          className={`square ${props.highlighted ? "highlight": ""}`}
          onClick={props.onClick} 
        >
          {props.value}
      </button>
  );
}

class Board extends React.Component {
  renderSquare(i) {
    return <Square
      value={this.props.squares[i]}
      highlighted={this.props.highlights[i]}
      onClick={() => { this.props.onClick(i) }}
      />;
  }

  render() {
    const squareIndexes = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8]
    ]

    const squares = squareIndexes.map(r => r.map(s => this.renderSquare(s)));
    const boardRows = [];

    for (let i = 0; i < 3; i++) {
      boardRows.push(
        <div className="board-row">
          {squares[i]}
        </div>
      );
    }

    return (
      <div>{boardRows}</div>
    );
  }
}

class Game extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      history: [{
        squares: Array(9).fill(null),
        col: 0,
        row: 0,
        highlights: Array(9).fill(false),
      }],
      xIsNext: true,
      stepNumber: 0,
      descending: false,
    };
  }

  handleClick(i) {
    const history = this.state.history.slice(0, this.state.stepNumber + 1);
    const current = history[history.length - 1]
    const squares = current.squares.slice()

    if (calculateWinner(squares).winner || squares[i]) {
      return;
    }

    squares[i] = this.state.xIsNext ? 'X' : 'O';

    this.setState({
      history: history.concat([{
        squares: squares,
        highlights: current.highlights,
        row: Math.floor(i / 3),
        col: i % 3,
      }]),
      stepNumber: history.length,
      xIsNext: !this.state.xIsNext,
    });
  }

  jumpTo(move) {
    this.setState({
      stepNumber: move,
      xIsNext: (move % 2) === 0,
    })
  }

  toggleSorting() {
    this.setState({
      descending: !this.state.descending,
    })
  }

  render() {
    const history = this.state.history;
    const current = history[this.state.stepNumber]
    const winner = calculateWinner(current.squares)

    const moves = history.map((step, move) => {
      const desc = move ?
        `Go to move #${move} at (${step.col}, ${step.row})` :
        'Go to game start';

      let descElem = desc
      if (move === this.state.stepNumber) {
        descElem = (<b>{desc}</b>)
      }

      return (
        <li key={move}>
          <button onClick={() => this.jumpTo(move)}>{descElem}</button>
        </li>
      );
    });

    if (this.state.descending) {
      moves.reverse();
    }

    let status;
    if (winner.winner) {
      status = 'Winner: ' + winner.winner;
      winner.highlightedSquares.forEach((i) => {
        current.highlights[i] = true;
      });
    } else {
      status = 'Next player: ' + (this.state.xIsNext ? 'X' : 'O');
    }

    return (
      <div className="game">
        <div className="game-board">
          <Board 
            squares={current.squares}
            onClick={(i) => { this.handleClick(i) }}
            highlights={current.highlights}
          />
        </div>
        <div className="game-info">
          <div>{status}</div>
          <ol>{moves}</ol>
          <button onClick={() => { this.toggleSorting() }}>Toggle sorting</button>
        </div>
      </div>
    );
  }
}

function calculateWinner(squares) {
  let winner;
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];

  let highlightedSquares;
  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i];
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      winner = squares[a];
      highlightedSquares = lines[i]
      break;
    }
  }

  if (squares.every(sq => sq !== null)) {
    winner = 'Draw';
  }

  return {
    winner,
    highlightedSquares
  }
}

// ========================================

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(<Game />);
